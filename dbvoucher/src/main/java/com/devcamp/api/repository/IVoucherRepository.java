package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.devcamp.api.model.CVoucher;

@Service
public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {

}
